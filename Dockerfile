FROM python:2-stretch
MAINTAINER yasser
WORKDIR /project
COPY . /project
#RUN apt-get update -y ;apt-get install libmariadb-dev -y 

RUN apt-get update; apt-get install python2.7-dev default-libmysqlclient-dev -y
RUN pip install --no-cache-dir -r /project/musicwalletproject/requirements.txt
EXPOSE 8000
CMD python musicwalletproject/manage.py runserver 0.0.0.0:8000 --settings=musicwalletproject.settings.development
